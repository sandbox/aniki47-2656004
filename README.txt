INTRODUCTION
------------
Pay with Pay.jp module provides integration of the Drupal Ecosystem along
with the Pay.jp payment gateway.


REQUIREMENTS
------------
This module requires the following modules:
 * Libraries (https://www.drupal.org/project/libraries)

And a PHP library:
 * Payjp PHP library
   (https://github.com/payjp/payjp-php),
   which can be downloaded as zip file easily.


INSTALLATION
------------
 1. Install as you would normally install a contributed Drupal module. See:
    https://www.drupal.org/documentation/install/modules-themes/modules-7
    for further information.
 2. Place the Payjp PHP library in the DRUPAL_ROOT/sites/all/libraries/
    directory. Rename the unzipped library to 'payjp'.


CONFIGURATION
-------------
 * Go to Administration » Configuration » Services » Payjp module settings:
   Set the Secret API Key accordingly.
   Choose the environment depending the upon, whether the site is in Development
   phase or production phase.


According to the modules you enabled, you can now start using the payment
gateway depending upon the project requirement

Further documentation about the Pay.jp payment gateway can be found at:
https://pay.jp/docs/api/?php#introduction


MAINTAINERS
-----------
The module is developed by: aniki47 (https://www.drupal.org/u/aniki47)
with support from : Genero Technologies, Inc.
