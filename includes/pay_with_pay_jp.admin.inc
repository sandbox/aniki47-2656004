<?php
/**
 * @file
 * Handles the admin form settings of the Pay with pay.jp form.
 */

/**
 * Settings form for Pay.jp.
 */
function pay_with_pay_jp_settings_form($form, &$form_state) {
  $form = array();
  $form['pay_jp_admin_form'] = array(
    '#type' => 'fieldset',
    '#title' => 'Pay.jp Payment Settings Form',
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['pay_jp_admin_form']['pay_with_pay_jp_api_test_key'] = array(
    '#type' => 'textfield',
    '#title' => t("Enter the Shop's Test Secret Key"),
    '#description' => t("Enter the test secret key received from Pay.jp (Key starting with 'sk_test_')"),
    '#size' => 60,
    '#maxlength' => 180,
    '#default_value' => variable_get('pay_with_pay_jp_api_test_key', ''),
  );
  $form['pay_jp_admin_form']['pay_with_pay_jp_api_live_key'] = array(
    '#type' => 'textfield',
    '#title' => t("Enter the Shop's Live Secret Key"),
    '#description' => t("Enter the live secret key received from Pay.jp (Key starting with 'sk_live_')"),
    '#size' => 60,
    '#maxlength' => 180,
    '#default_value' => variable_get('pay_with_pay_jp_api_live_key', ''),
  );
  $form['pay_jp_admin_form']['pay_with_pay_jp_environment_setting'] = array(
    '#type' => 'checkbox',
    '#title' => 'Testing mode',
    '#default_value' => variable_get('pay_with_pay_jp_environment_setting', FALSE),
  );
  return system_settings_form($form);
}
