<?php

/**
 * @file
 * Pay with pay.jp primary module file.
 */

/**
 * Implements hook_libraries_info().
 */
function pay_with_pay_jp_libraries_info() {
  // Payjp client (PHP) library.
  // Expected to be extracted into 'sites/all/libraries/payjp'.
  $libraries['payjp'] = array(
    'name' => 'Payjp Library',
    'vendor url' => 'https://pay.jp/',
    'download url' => 'https://github.com/payjp/payjp-php',
    'version callback' => 'pay_with_pay_jp_version_callback',
    'files' => array(
      'php' => array('init.php'),
    ),
  );

  return $libraries;
}

/**
 * A dummy function validating the version of PHP Library.
 */
function pay_with_pay_jp_version_callback() {
  return TRUE;
}

/**
 * Implements hook_help().
 */
function pay_with_pay_jp_help($path, $arg) {
  switch ($path) {
    case 'admin/help#pay_with_pay_jp':
      $output = '';
      $output .= '<p>' . t('The Pay with Pay.jp module provides an integration with the <a href="@drupal">Pay.jp</a> payment gateway of Japan . The module provides a simple interface which can be extended to any use case as per the project requirement.', array('@drupal' => 'https://pay.jp/')) . '</p>';
      $output .= '<p>' . t('The Pay with Pay.jp <a href="@settings">settings page</a> allows you to set the secret API key received for each account on Pay.jp', array('@settings' => url('admin/config/services/payjp_settings'))) . '</p>';
      $output .= '<p>' . t('The module uses a php library available <a href="@lib">here</a>. Download the library, unzip it and place it in the sites/all/library folder and rename the folder to "payjp".', array('@lib' => url('https://github.com/payjp/payjp-php'))) . '</p>';
      return $output;
  }
}

/**
 * Implements hook_menu().
 */
function pay_with_pay_jp_menu() {
  $items = array();
  $items['payformpage'] = array(
    'title' => 'Pay.jp',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pay_with_pay_jp_form'),
    'access callback' => array('user_access'),
    'access arguments' => array('access pay_with_pay_jp'),
    'type' => MENU_NORMAL_ITEM,
  );
  $items['admin/config/services/payjp_settings'] = array(
    'title' => 'Pay.jp Settings',
    'description' => 'Configure the API key settings for Pay with Pay.jp module.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pay_with_pay_jp_settings_form'),
    'access callback' => array('user_access'),
    'access arguments' => array('administer pay_with_pay_jp'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'includes/pay_with_pay_jp.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function pay_with_pay_jp_permission() {
  return array(
    'administer pay_with_pay_jp' => array(
      'title' => t('Administer Pay with Pay.jp settings'),
    ),
    'access pay_with_pay_jp' => array(
      'title' => t('Access Pay.jp page for payment'),
    ),
  );
}

/**
 * Master form which collects all the details required.
 *
 * @see pay_with_pay_jp_form_validate()
 * @see pay_with_pay_jp_form_submit()
 */
function pay_with_pay_jp_form($form, &$form_state) {

  // To check whether the payjp library is loaded or not.
  $flag = FALSE;

  if (($library = libraries_detect('payjp')) && !empty($library['installed'])) {
    // The library is installed. Awesome!
    $flag = TRUE;
    $apikey = '';
    libraries_load('payjp');
    if (variable_get('pay_with_pay_jp_environment_setting')) {
      // Test environment.
      $apikey = variable_get('pay_with_pay_jp_api_test_key', '');
    }
    else {
      // Live environment.
      $apikey = variable_get('pay_with_pay_jp_api_live_key', '');
    }
    if (isset($apikey) && $apikey != '') {
      \Payjp\Payjp::setApiKey($apikey);
    }
    else {
      $flag = FALSE;
      form_set_error('error', t('Please configure the settings for the Pay with
        Pay.jp module <a href="@settings">here</a>.', array('@settings' => url('admin/config/services/payjp_settings'))));
    }
  }
  else {
    // This contains a detailed (localized) error message.
    $error_message = $library['error message'];
    form_set_error('error', $error_message);
  }

  $form['amount_form'] = array(
    '#type' => 'fieldset',
    '#title' => t('Payment Amount'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['amount_form']['amount'] = array(
    '#type' => 'textfield',
    '#title' => t('Amount'),
    '#size' => 6,
    '#maxlength' => 6,
    '#description' => t('Enter the amount to be paid (￥).'),
    '#required' => TRUE,
  );

  $form['card_form'] = array(
    '#type' => 'fieldset',
    '#title' => t('Card Details'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['card_form']['number'] = array(
    '#type' => 'textfield',
    '#title' => t('Card Number'),
    '#size' => 20,
    '#maxlength' => 16,
    '#description' => t('Enter the card number'),
    '#required' => TRUE,
  );
  $form['card_form']['cvc'] = array(
    '#type' => 'textfield',
    '#size' => 5,
    '#maxlength' => 4,
    '#title' => t('CVC Number'),
    '#description' => t('Enter the cvc number'),
    '#required' => TRUE,
  );
  $form['card_form']['exp_month'] = array(
    '#type' => 'textfield',
    '#title' => t('Expiry Month'),
    '#size' => 3,
    '#maxlength' => 2,
    '#description' => t('Enter the expiry month'),
    '#required' => TRUE,
  );
  $form['card_form']['exp_year'] = array(
    '#type' => 'textfield',
    '#title' => t('Expiry Year'),
    '#size' => 5,
    '#maxlength' => 4,
    '#description' => t('Enter the expiry year'),
    '#required' => TRUE,
  );
  $form['card_form']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Full Name'),
    '#size' => 20,
    '#maxlength' => 30,
    '#description' => t('Enter the card holders name'),
  );
  if ($flag == TRUE) {
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Submit'),
    );
  }
  return $form;

}

/**
 * Validation function for pay_with_pajp_form.
 *
 * It also creates transaction token.
 *
 * @see pay_with_pay_jp_form()
 * @see pay_with_pay_jp_form_submit()
 */
function pay_with_pay_jp_form_validate($form, &$form_state) {
  $card_number = $form_state['values']['number'];
  $card_cvc = $form_state['values']['cvc'];
  $card_month = $form_state['values']['exp_month'];
  $card_year = $form_state['values']['exp_year'];
  $card_name = $form_state['values']['name'];

  $card = array(
    "number" => $card_number,
    "cvc" => $card_cvc,
    "exp_month" => $card_month,
    "exp_year" => $card_year,
    "name" => $card_name,
  );

  try {
    $token = \Payjp\Token::create(array(
      "card" => $card,
    ));
    $token_converted = array_values((array) $token);
  }
  catch (Exception $e) {
    $param = $e->param;
    $param = substr($param, 5, -1);
    form_set_error($param, t('Invalid card details.'));
  }
  if (isset($token_converted[1]['id'])) {
    $form_state['values']['token_number'] = $token_converted[1]['id'];
  }
}

/**
 * Handles what to do with the submitted form depending on the data passed.
 *
 * @see pay_with_pay_jp_form()
 * @see pay_with_pay_jp_form_validate()
 */
function pay_with_pay_jp_form_submit($form, &$form_state) {
  $amount = (int) $form_state['values']['amount'];
  if (isset($form_state['values']['token_number'])) {
    $token = $form_state['values']['token_number'];
    try {
      $charge = \Payjp\Charge::create(array(
        'card' => $token,
        'amount' => $amount,
        'currency' => 'jpy',
      ));
      if ($charge) {
        drupal_set_message(t('Your payment is complete.'), 'status', FALSE);
      }
    }
    catch (Exception $e) {
      form_set_error('error', t('Something went wrong with the server. Please cont
        act the Site Administrator for further details.'));
    }
  }
  else {
    form_set_error('error', t('Something went wrong with the server. Please cont
        act the Site Administrator for further details.'));
  }
}
